
const URL = 'http://localhost:8080/api'
const DELAY = 1000 * 15

const createCol = (data) => (tr, key) => {
	const td = document.createElement('td')
	td.innerHTML = data[key]
	tr.appendChild(td)
	return tr
}

const createRow = (table, item) => {
	const tr = Object.keys(item)
		.reduce(createCol(item), document.createElement('tr'))
	table.appendChild(tr)
	return table
}

const createRenderer = (selector) => {
	const wrapper = document.querySelector(selector)
	return (data = []) => {
		const table = document.createElement('table')
		data.reduce(createRow, table)
		wrapper.innerHTML = ''
		wrapper.appendChild(table)
	}
}

const parseData = (data) => data.stock.map(item => ({
	name: item.name,
	amount: item.price.amount,
	volume: item.volume
}))

const getData = (url) => fetch(url).then(res => res.json()).then(parseData)
const createUpdater = (url, render) => () => getData(url).then(render)


const main = () => {
	const render = createRenderer('#table-wrapper')
	const update = createUpdater(URL, render)

	document.getElementById('btn_refresh')
		.addEventListener('click', update)

	setInterval(update, DELAY)

	update()
}

main()