const axios = require('axios')
const express = require('express')
const config = require('./config.js')

const app = express()

const proxy = (req, res) =>
	axios.get(config.proxy.from)
	.then(fromRes => res.send(fromRes.data))

app.use(express.static('public'))
.get('/api', proxy)

const port = config.server.port
app.listen(port, console.log.bind(console, `Server is listen port ${port}`))