module.exports = {
	server: {
		port: 8080
	},
	proxy: {
		from: 'http://phisix-api3.appspot.com/stocks.json'
	}
}